#!/bin/bash

# Edit the script file which contains starters' 
# variable assignment and dialogs to replace the
# initial ones (bulbasaur, squirtle, charmander)
# with random ones.

possible_starters=("SPECIES_BULBASAUR" "SPECIES_CHARMANDER" "SPECIES_SQUIRTLE" "SPECIES_CATERPIE" "SPECIES_WEEDLE" "SPECIES_PIDGEY" "SPECIES_NIDORAN_F" "SPECIES_NIDORAN_M" "SPECIES_ZUBAT" "SPECIES_ODDISH" "SPECIES_POLIWAG" "SPECIES_ABRA" "SPECIES_MACHOP" "SPECIES_BELLSPROUT" "SPECIES_GEODUDE" "SPECIES_GASTLY" "SPECIES_HORSEA" "SPECIES_CHIKORITA" "SPECIES_CYNDAQUIL" "SPECIES_TOTODILE" "SPECIES_PICHU" "SPECIES_CLEFFA" "SPECIES_IGGLYBUFF" "SPECIES_MAREEP" "SPECIES_HOPPIP" "SPECIES_TREECKO" "SPECIES_TORCHIC" "SPECIES_MUDKIP" "SPECIES_WURMPLE" "SPECIES_LOTAD" "SPECIES_SEEDOT" "SPECIES_TRAPINCH" "SPECIES_SPHEAL" "SPECIES_AZURILL" "SPECIES_SLAKOTH" "SPECIES_WHISMUR" "SPECIES_ARON" "SPECIES_RALTS")

# Shuffle the possible_starters array using Fisher-Yates algorithm
shuffle() {
  local i=$(( ${#possible_starters[@]} - 1 ))
  while [ $i -gt 0 ]; do
    local j=$(( RANDOM % (i + 1) ))
    local temp=${possible_starters[i]}
    possible_starters[i]=${possible_starters[j]}
    possible_starters[j]=$temp
    i=$(( i - 1 ))
  done
}
shuffle
bulb_replacement=${possible_starters[0]}
squi_replacement=${possible_starters[1]}
char_replacement=${possible_starters[2]}

# Modify the script file
# Logic:
# - search for the header refering to one of the original starters,
# - count four rows from there,
# - replace the current starter (which can already be a random one) with a new random one
script_filename="data/maps/PalletTown_ProfessorOaksLab/scripts.inc"
scriptfile_search_token_bulb="PalletTown_ProfessorOaksLab_EventScript_BulbasaurBall::"
scriptfile_search_token_squi="PalletTown_ProfessorOaksLab_EventScript_SquirtleBall::"
scriptfile_search_token_char="PalletTown_ProfessorOaksLab_EventScript_CharmanderBall::"
temp_file=$(mktemp)
countdown=-1
next_repl=""
while IFS= read -r line; do
    countdown=$((countdown-1))
  if [[ $line == *"$scriptfile_search_token_bulb"* ]]; then
    countdown=4
    next_repl=$bulb_replacement
  fi
  if [[ $line == *"$scriptfile_search_token_squi"* ]]; then
    countdown=4
    next_repl=$squi_replacement
  fi
  if [[ $line == *"$scriptfile_search_token_char"* ]]; then
    countdown=4
    next_repl=$char_replacement
  fi
  if [[ $countdown == 0 ]]; then
    IFS=' ' read -r -a tokens <<< "$line"
    tokens[2]="${next_repl}"
    updated_line="${tokens[*]}"
    echo "$updated_line" >> "$temp_file"
  else
    echo "$line" >> "$temp_file"
  fi
done < "$script_filename"

mv "$temp_file" "$script_filename"

# Modify the text file (similar logic)
text_filename="data/maps/PalletTown_ProfessorOaksLab/text.inc"
textfile_search_token_bulb="PalletTown_ProfessorOaksLab_Text_OakChoosingBulbasaur::"
textfile_search_token_squi="PalletTown_ProfessorOaksLab_Text_OakChoosingSquirtle::"
textfile_search_token_char="PalletTown_ProfessorOaksLab_Text_OakChoosingCharmander::"
temp_file=$(mktemp)
countdown=-1
next_repl=""
while IFS= read -r line; do
    countdown=$((countdown-1))
  if [[ $line == *"$textfile_search_token_bulb"* ]]; then
    countdown=4
    next_repl=$bulb_replacement
  fi
  if [[ $line == *"$textfile_search_token_squi"* ]]; then
    countdown=4
    next_repl=$squi_replacement
  fi
  if [[ $line == *"$textfile_search_token_char"* ]]; then
    countdown=4
    next_repl=$char_replacement
  fi
  if [[ $countdown == 3 ]]; then
    updated_line="    .string \"So ${next_repl#SPECIES_} is your choice.\\n\""
    echo "$updated_line" >> "$temp_file"
  elif [[ $countdown == 0 ]]; then
    updated_line="    .string \"${next_repl#SPECIES_}?\$\""
    echo "$updated_line" >> "$temp_file"
  else
    echo "$line" >> "$temp_file"
  fi
done < "$text_filename"

mv "$temp_file" "$text_filename"
